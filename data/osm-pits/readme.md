
# Austrlian mining pits found in Open Street Maps mining areas

## creation:

- created with scripts at `https://gitlab.anu.edu.au/u1046630/brownfield-phes-mining`

## files:

- `osm-aus-pits.csv`: spreadsheet readable, contains all the same data except for the shapes
- `osm-aus-pits.*` (everything else): readable in GIS software (QGIS/ArcGIS), contains all the data including the shapes.

## attributes:

- mine_id: unique to the mining area polygon, assigned when I downloaded them from OSM.
- mine_name: from OSM
- mine_area: m2
- pit_depth: m
- pit_lat / pit_lon: WGS84 coordinates for the lowest part of the pit
- pit_top / pit_bottom: elevation in m
- pit_volume: GL
- pit_area: m2
- 80_altitude: elevation in m of 80% of the from pit_bottom to pit_top (so 100_altitude is the same as pit_top)
- 80_volume: GL, volume of water used to fill to water level to 80_altitude (so 100_volume is the same as pit_volume)

## caveats:

- the pits were found using NASA ASTER GDEM data, which is based on satelite imagery taken between 2000 and 2013, newer mining pits have hence not been found.
- the volume of a mining pit is the volume at which water starts to overflow, no dam walls are considered.
- pits under 0.1GL were discarded
- only one pit per mining area is saved (the largest by 100% volume).
- the polygons of the pits in the shapefiles are approximate convex hulls, so often go outside the actual mining areas. 
