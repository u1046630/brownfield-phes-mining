
# Raw Data

## Aus Mining Altas
- data from here: `http://www.australianminesatlas.gov.au/mapping/downloads.html`
- 595 history and 525 operating

## Geoscience Australia
- downloaded "Geodata Topo Series 3" (the 1:250000 topo maps of Aus) from `https://www.ga.gov.au/scientific-topics/national-location-information/topographic-maps-data/digital-topographic-data`
- detailed info on minedata format `http://www.ga.gov.au/mapspecs/topographic/v6/appendixA_files/Industry.html#Industry%20Storage%20Tank%20Point` (importantly, it says 1:250000 topos only contain mineares > 140625 sq km).
- minearea data found in `Vector_Data > Infrastructure.mdb`. Format: ESRI Personal Geodatabase, which can't be opened with QGIS. So convert to CSV with DBeaver Community Edition (Database > New Database Connection > MS Access. Then Tables > MineAreas > right click > Export Data > CSV File > Binaries Inline). 
- `scripts/mineareas-csv-to-shapefile.py` converts CSV file to shapefile that can be imported into QGIS.

# OpenTopoData

- opentopodata.ord has public API for querying the aster30m dataset (global with 30m res).
- has request limits, but can download and run locally in docker



# Kanmantoo copper mine

- Operating copper mine in SA, was going to be developed into pumped hydro, so is a good test case to start with.
- coords: -35.0919, 139.0054 (lands straight in pit) -35.091660, 139.005675
- Just to west of pit on top of tailings: 223m (-35.089392,138.999303)
- Right in pit: 107m (-35.091753,139.0060340
- This matches roughly with Google maps terrain view

# MUSWELLBROOK NO2 MINE

Also a good one. has a big hole and fits nicely with one lat/long square


# Geoscience Australia map

- Mine Area 
- http://www.ga.gov.au/mapspecs/topographic/v6/appendixA_files/Industry.html#Industry%20Storage%20Tank%20Point


