#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 21:25:23 2020

@author: liam
"""

# assumes "../data/mine-results.pickle" exist from running `extract-elevation.py`

import pickle
import numpy as np, geopandas as gpd
from rasterio.plot import show
import pyproj
from scipy.ndimage.measurements import label

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D

shapefile = gpd.read_file('../layers/mineareas.shp')
geod = pyproj.Geod(ellps='WGS84')
    
# values: 2D numpy array
def surface_plot(values, title='', azim=0):
   # Z = np.float(values)
    Z = values
    X = np.arange(values.shape[0])
    Y = np.arange(values.shape[1])
    X,Y = np.meshgrid(X,Y,indexing='ij')
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.view_init(elev=0., azim=azim)
    ax.set_title(title)
    ax.set_zlabel('elevation (m)')
    surf = ax.plot_surface(X,Y,Z, cmap='ocean')
    plt.show()
    
def create_pics(results):
    for objectid, quality in results[:20]:
        row = shapefile[shapefile.objectid == objectid].iloc[0]
        polygon = row.geometry
        bbox = polygon.bounds
        x = (bbox[0]+bbox[2])/2
        y = (bbox[1]+bbox[3])/2
        title = '{}\n({:.3f},{:.3f})'.format(row['name'], y, x)
        values = get_raster_values(polygon)
        surface_plot(values, title=title, azim=0)
        plt.savefig('../output/3d-plots/{}-sideA.png'.format(objectid), bbox_inches='tight')
        plt.close()
        surface_plot(values, title=title, azim=90)
        plt.savefig('../output/3d-plots/{}-sideB.png'.format(objectid), bbox_inches='tight')
        plt.close()
        
def local_minima(array):
    return (
        (array <= np.roll(np.roll(array, -1, 0), -1, 1)) & 
        (array <= np.roll(np.roll(array, -1, 0),  1, 1)) &  
        (array <= np.roll(np.roll(array,  1, 0), -1, 1)) &  
        (array <= np.roll(np.roll(array,  1, 0),  1, 1)) &
        (array <= np.roll(array,  1, 0)) & 
        (array <= np.roll(array, -1, 0)) & 
        (array <= np.roll(array,  1, 1)) & 
        (array <= np.roll(array, -1, 1))
    )
    
def get_area_values(polygon):
    polygon_json = [mapping(polygon)] # convert to GeJSON format
    bbox_json = [mapping(box(*polygon.bounds))]
    # get bounding box as polygon, then use `mask` to crop raster to 
    # the bounding box
    # there's probably a much neater way!
    raster = get_raster_poly(polygon)
    values,_ = mask(raster, polygon_json, crop=True, nodata=-1)
    area_mask = values[0,1:-1,1:-1] != -1 # True inside mining area
    values,_ = mask(raster, bbox_json, crop=True)
    values = values[0,1:-1,1:-1] # elevation for whole bounding box
    assert(values.shape == area_mask.shape)
    return values, area_mask

def calc_water_level(AA, BB, x, y):
    # fill the elevation profile (AA) with water until the local minima (x,y)
    # is connected to the boundary (BB)
    
    # now do a binary search for the elevation of the water level
    z_min, z_max = AA[x, y], np.max(AA)
    while z_max - z_min > 1:
        z_water = int((z_min+z_max)/2)
        CC,_ = label(np.bitwise_or(BB, AA <= z_water)) # CC for connected components
        
        if CC[0,0] == CC[x,y]: # overflow
            z_max = z_water
        else: # no overflow
            z_min = z_water
            
    z_water = z_min # z_water is the highest water level without overflow
    CC,_ = label(np.bitwise_or(BB, AA <= z_water)) # connected components analysis
    CC = CC == CC[x,y] # select just the water filled component
    
    # still overflowing, likely not a true minima (equal value to neighbour)
    if CC[0,0] == CC[x,y]: 
        assert(z_water == AA[x, y])
        z_water = None
        
    return CC, z_water

    
        
        
with open('../data/mine-results.pickle', 'rb') as f:
    results = pickle.load(f)
    
volumes = []

for index in range(len(results)):
    
    objectid = results[index][0]
    #print('{}...'.format(objectid))
    
    if objectid != 1070:
        continue
   
    row = shapefile[shapefile.objectid == objectid].iloc[0]
    name = row['name']
    polygon = row.geometry
    bbox = polygon.bounds
    long = (bbox[0] + bbox[2])/2
    lat = (bbox[1] + bbox[3])/2
    _,_,distperlong = geod.inv(long-0.5, lat, long+0.5, lat) # m per degree of long
    _,_,distperlat = geod.inv(long, lat-0.5, long, lat+0.5) # m per degree of lat
    elevation, area_mask = get_area_values(polygon)
    volume_per_cubic_unit = 1.0 * distperlat/3600 * distperlong/3600 / 1e6 # GL
    #surface_plot(elevation, title=objectid)
    
    # Approach:
    # Pretend there is water around the boarder and everywhere that elevation < z_water. 
    # Pick a local minima (x,y) to fill with water. 
    # Calculate the connected componets of the water + boarder for some water level.
    # If x,y is in the same connected component as 0,0 (the boarder) than the 
    # local minima has overflowed out of the bounding box of the mining area.
    # Do a binary search to find the highest z_water that doesn't overflow.
    # Calculate the volume of the newly formed reservoir.
    # Do this for every local minima in the mining area, skipping minima that get
    # flooded when filling other minima.
    
    # AA is elevation profile, with a 1 pixel wide boarder of 0 elevation
    s0,s1 = elevation.shape
    AA = np.zeros((s0+2, s1+2), dtype=elevation.dtype)
    AA[1:-1,1:-1] = elevation
    
    # MM == True inside the mining area
    MM = np.zeros(AA.shape, dtype='bool')
    MM[1:-1,1:-1] = area_mask
    
    ## BB == True around the boarder (1 pixel wide)
    BB = np.ones(AA.shape, dtype='bool') # B for bounding box
    BB[1:-1, 1:-1] = False
    
    # PP == True at a local minima inside the mining area
    PP = np.bitwise_and(local_minima(AA), MM) 
    
    cache = (0, None, None)
    
    while np.sum(PP) > 0:
        
        minima = np.where(PP)
        x, y = minima[0][0], minima[1][0]
        z = AA[x,y]
        CC, z_water = calc_water_level(AA, BB, x, y)
        if z_water == None:
            #print('Minima ({},{}): no pit found'.format(x, y))
            PP[x,y] = False
        else:
            depth = CC * (z_water - AA)
            volume = np.sum(depth) * volume_per_cubic_unit
            if volume > cache[0]:
                cache = volume, x, y
            #print('Minima ({},{}): from {}m to {}m. {:.2f}GL'.format(x, y, z, z_water, volume))
            PP = np.bitwise_and(PP, np.bitwise_not(CC)) # skip all the other minima that got flooded
        
    is_pit = True
    if cache[1] == None:
        volume = 0.0
        is_pit = False
    else:
        volume, x, y = cache
        CC, z_water = calc_water_level(AA, BB, x, y)
        depth = CC * (z_water - AA)
    print('{:4}: pit: {}, volume: {:6.2f}GL, "{}"'.format(objectid, is_pit, volume, name))
    
    volumes.append((is_pit, volume, objectid, name))



