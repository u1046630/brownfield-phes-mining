#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 21:09:22 2020

@author: liam
"""

import rasterio
from rasterio.mask import mask
from rasterio.merge import merge
from rasterio.plot import show

import math, numpy as np, geopandas as gpd
import pickle

from shapely.geometry import mapping
from shapely.geometry import box

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D

# find aster30m tif file that covers this lat/long
def aster30m_fname(x, y):
    c1 = 'N' if y >= 0 else 'S'
    c2 = 'E' if x >= 0 else 'W'
    x = abs(math.floor(x))
    y = abs(math.ceil(y))
    return '/home/liam/Documents/aster30m/rawdata/{}{}{}{}.tif'.format(c1, y, c2, x)

# polygon: shapely.geometry.polygon.Polygon (mine area)
# out: numpy.ndarray of values from rasters that cover (elevation)
def get_raster_poly(polygon):
    minX,minY,maxX,maxY = polygon.bounds
    minX,maxX = math.floor(minX), math.floor(maxX)
    minY,maxY = math.floor(minY), math.floor(maxY)
    fnames = set([
            aster30m_fname(minX,minY),
            aster30m_fname(minX,maxY),
            aster30m_fname(maxX,minY),
            aster30m_fname(maxX,maxY),
    ])
    srcs = [rasterio.open(fname) for fname in fnames]
    if len(srcs) == 1:
        return srcs[0]
    
    # damn we have to stitch multiple rasters together
    data,transform = merge(srcs)
    meta = srcs[0].meta.copy()
    meta.update({
            'height': data.shape[1],
            'width': data.shape[2],
            'transform': transform,
    })
    with rasterio.open('temp', 'w', **meta) as f:
        f.write(data)
    return rasterio.open('temp')
    
def phes_quality(values):
    return np.max(values) - np.min(values)

def get_raster_values(polygon):
    bbox = box(*polygon.bounds) # bounding box (shapely polygon)
    bbox_json = [mapping(bbox)] # convert to GeJSON format

    # get bounding box as polygon, then use `mask` to crop raster to 
    # the bounding box
    # there's probably a much neater way!
    raster = get_raster_poly(bbox)
    values,transform = mask(raster, bbox_json, crop=False)
    values = values[0,1:-1,1:-1] # 2D np array
    return values
    

results = {}

shapefile = gpd.read_file('../layers/mineareas.shp')
polygons = shapefile.geometry.values

for index, row in shapefile.iterrows():
    print('calcing {}...'.format(index))
    polygon = row.geometry
    values = get_raster_values(polygon)
    results[int(row.objectid)] = phes_quality(values)

results = sorted(results.items(), key = lambda x: -x[1]) # sort by phes_quality

with open('../data/mine-results.pickle', 'wb') as f:
    pickle.dump(results, f)

    
    
