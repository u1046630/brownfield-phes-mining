# -*- coding: utf-8 -*-

import requests, pyproj
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D

second = 1/3600
radius = 500 # radius to scan from lat,long in meters

mines = {
    'kanmantoo': (-35.0919, 139.0054),
    'loy yang': (-38.235, 146.564),        
}

lat,long = mines['loy yang']

# round it to the closed arcsecond
lat = round(lat*3600)/3600
long = round(long*3600)/3600

url = "http://localhost:5000/v1/aster30m?locations="
max_requests = 200

def get_elevations(coords):
    elevations = []
    while len(coords):
        batch = coords[:max_requests]
        coords = coords[max_requests:]
        query = '|'.join(['{},{}'.format(lat,long) for lat,long in batch])
        response = requests.get(url + query)
        if response.status_code != 200:
            print(response)
            exit()
        results = response.json()['results']
        elevations.extend([x['elevation'] for x in results])
    return elevations

geod = pyproj.Geod(ellps='WGS84')
_,_,distperlong = geod.inv(long-0.5, lat, long+0.5, lat) # m per degree of long
print(distperlong)
_,_,distperlat = geod.inv(long, lat-0.5, long, lat+0.5) # m per degree of lat
print(distperlat, distperlong)


xrad = radius/distperlong # x radius in degrees long
yrad = radius/distperlat # y radius in degrees lat
x = np.arange(-xrad, xrad, second, dtype='float64')
y = np.arange(-yrad, yrad, second, dtype='float64')
nx = len(x)
ny = len(y)

coords = []
for i in x:
    for j in y:
        coords.append((lat + j, long + i))
        
elevations = get_elevations(coords)    
z = np.reshape(np.array(elevations), (nx,ny))

x *= distperlong
y *= distperlat

fig = plt.figure()
ax = fig.gca(projection='3d')

ax.set_xlabel('x (m). longitude.')
ax.set_ylabel('y (m). latitude.')
ax.set_zlabel('z (m). elevation.')

X,Y = np.meshgrid(x, y, indexing='ij')
# Plot the surface.
surf = ax.plot_surface(X,Y,z, cmap=cm.ocean_r)
#cmap=cm.coolwarm, linewidth=10, antialiased=False

plt.show()






















