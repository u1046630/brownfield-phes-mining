#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 21:09:22 2020

@author: liam
"""

import rasterio
from rasterio.mask import mask
from rasterio.merge import merge
from rasterio.plot import show

import math, numpy as np, geopandas as gpd
import pickle
import pyproj

from shapely.geometry import mapping
from shapely.geometry import box

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D

### input params
min_area = 50e3 # minimum mining area size in m2

### globals
geod = pyproj.Geod(ellps='WGS84')

def latlon_scaling(lat, lon):
    _,_,m_per_lat = geod.inv(lon, lat-0.5, lon, lat+0.5) # m per degree of lat
    _,_,m_per_lon = geod.inv(lon-0.5, lat, lon+0.5, lat) # m per degree of long
    return m_per_lat, m_per_lon

def calc_area(poly):
    # shapely is not GIS, so polygon.area calculates area on euclidean plane
    bounds = poly.bounds
    lat = (bounds[1] + bounds[3])/2
    lon = (bounds[0] + bounds[2])/2
    m_per_lat, m_per_lon = latlon_scaling(lat, lon)
    return poly.area * m_per_lat * m_per_lon








# polygon: shapely.geometry.polygon.Polygon (mine area)
# out: numpy.ndarray of values from rasters that cover (elevation)
def get_raster_poly(polygon):
    minX,minY,maxX,maxY = polygon.bounds
    minX,maxX = math.floor(minX), math.floor(maxX)
    minY,maxY = math.floor(minY), math.floor(maxY)
    fnames = set([
            aster30m_fname(minX,minY),
            aster30m_fname(minX,maxY),
            aster30m_fname(maxX,minY),
            aster30m_fname(maxX,maxY),
    ])
    srcs = [rasterio.open(fname) for fname in fnames]
    if len(srcs) == 1:
        return srcs[0]
    
    # damn we have to stitch multiple rasters together
    data,transform = merge(srcs)
    meta = srcs[0].meta.copy()
    meta.update({
            'height': data.shape[1],
            'width': data.shape[2],
            'transform': transform,
    })
    with rasterio.open('temp', 'w', **meta) as f:
        f.write(data)
    return rasterio.open('temp')

def get_raster_values(polygon):
    bbox = box(*polygon.bounds) # bounding box (shapely polygon)
    bbox_json = [mapping(bbox)] # convert to GeJSON format

    # get bounding box as polygon, then use `mask` to crop raster to 
    # the bounding box
    # there's probably a much neater way!
    raster = get_raster_poly(bbox)
    #values,transform = mask(raster, bbox_json, crop=False)
    #values = values[0,1:-1,1:-1] # 2D np array
    return None
    

results = {}

shapefile = gpd.read_file('../data/osm-aus-mines.shp')

for index, row in shapefile.iterrows():
    poly = row.geometry
    if poly == None:
        continue
    if calc_area(poly) < min_area:
        continue
    print('[*] analysing row {}'.format(index))
    values = get_raster_values(poly)
    
    
