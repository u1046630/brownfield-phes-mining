#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 21 17:04:36 2020

@author: liam

Takes one npz file containing latitude & longitude arrays,
produces a copy npz file with two extra arrays, "elev" and "res"
elevation and resolution in meters. 
TODO: multithread, send requests all at once
"""

import requests, gevent
import pandas as pd, numpy as np

api_key_fname = '/home/liam/.googleapikey'
input_fname = './tom-price-coords.npz'
output_fname = './tom-price-elevation.npz'
base_url = 'https://maps.googleapis.com/maps/api/elevation/json'
column_lat = 'lat'
column_lon = 'lon'

def get_elevation(key, lat, lon):
    # according to Google Elevation API docs
    # sending requests one at a time returns better resolution results
    params = {'locations': f'{lat},{lon}', 'key': api_key}
    r = requests.get(base_url, params=params)
    r.raise_for_status()
    obj = r.json()
    status = obj.get('status')
    if status != 'OK':
        msg = obj['error_message']
        raise Exception(f'[!] Error: {msg}')
    result = obj.get('results')[0]
    return result.get('elevation'), result.get('resolution')

with open(api_key_fname, 'r') as f:
    api_key = f.read()
    
f = np.load(input_fname)
lats = f[column_lat]
lons = f[column_lon]
shape = lats.shape
assert(shape == lons.shape)
res = np.full(lats.shape, np.nan)
elev = np.full(lats.shape, np.nan)

coords = list(zip(lats.flatten(), lons.flatten()))[:10]
print(f'[*] sending {len(coords)} requests to Google Elevation API...')
print(f'[*] cost will be: ${len(coords)*5/1000:.2f}')
input('[*] press enter to continue, ctrl-C to abort')
print('[*] continuing')

jobs = [gevent.spawn(get_elevation,api_key,lat,lon) for lat,lon in coords]
gevent.joinall(jobs)
print('[*] complete. saving...')
elev = np.array([job.value[0] for job in jobs]).reshape(shape)
res = np.array([job.value[1] for job in jobs]).reshape(shape)
print('[*] done.')

np.savez(output_fname, **f, elev=elev, res=res)