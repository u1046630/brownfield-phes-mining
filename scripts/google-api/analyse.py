#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 24 20:10:03 2020

@author: liam
"""

import numpy as np
import matplotlib.pyplot as plt
import rasterio

input_fname = './tom-price-elevation.npz'
tif_fname = '/media/liam/158E22720D8D22AE/sync/gdem-tiles/S23E117.tif'
LAT, LON = -23, +117

f = np.load(input_fname)
lat, lon, elev_google = f['lat'], f['lon'], f['elev']

# appears that TIF file is indexed from top left corner
i1 = 3600 - int(round((lat.min() - LAT) * 3600))
i2 = 3600 - int(round((lat.max() - LAT) * 3600))
j1 = int(round((lon.min() - LON) * 3600))
j2 = int(round((lon.max() - LON) * 3600))

#lat, lon = np.meshgrid(np.linspace(LAT,LAT+1,3601,endpoint=True), np.linspace(LON,LON+1,3601,endpoint=True))
with rasterio.open(tif_fname) as f:
    elev_aster = f.read()[0, i1:i2-1:-1, j1:j2+1].transpose()

fig = plt.figure()

ax = plt.subplot(121, projection='3d')
ax.set_title('ASTER DGEM v3')
ax.plot_surface(-lat, lon, elev_aster, color='red')
ax.set_zlim([600, 900])
ax.set_xlabel('latitude')
ax.set_ylabel('longitude')
ax.set_zlabel('elevation')

ax = plt.subplot(122, projection='3d')
ax.set_title('Google Elevation API')
ax.plot_surface(-lat, lon, elev_google, color='blue')
ax.set_zlim([600, 900])
ax.set_xlabel('latitude')
ax.set_ylabel('longitude')
ax.set_zlabel('elevation')
#ax.legend(['Google', 'ASTER'])