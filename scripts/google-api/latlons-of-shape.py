#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 23 17:26:10 2020

@author: liam
"""

import numpy as np
import pandas as pd, geopandas as gpd

input_fname = '../../data/osm-pits/osm-aus-pits.shp'
name = 'Mount Tom Price mine'
output_fname = 'tom-price-coords.npz'
res = 1/3600 # 1 arc second between lat lon points

df = gpd.read_file(input_fname) 
shape = df[df['mine_name'] == name].iloc[0].geometry

lon_min, lat_min, lon_max, lat_max = shape.bounds
lon_min = np.ceil(lon_min/res)*res # only get coords INSIDE bounds
lat_min = np.ceil(lat_min/res)*res

lats, lons = np.meshgrid(
    np.arange(lat_min, lat_max, res), 
    np.arange(lon_min, lon_max, res), 
)
np.savez(output_fname, lat=lats, lon=lons)