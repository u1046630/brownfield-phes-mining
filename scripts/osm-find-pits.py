#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 21:25:23 2020

@author: liam
"""

# About: 
# input_fname is a shapefile of mining areas in Australia
# aster30m_dir contains raster files with 30m resolution of elevation
# using elevation data, find mining pits inside the mining areas
# and save them to output_fname (shapefile)

# Approach:
# Pretend there is water around the boarder and everywhere that elevation < z_water. 
# Pick a local minima (x,y) to fill with water. 
# Calculate the connected componets of the water + boarder for some water level.
# If x,y is in the same connected component as 0,0 (the boarder) than the 
# local minima has overflowed out of the bounding box of the mining area.
# Do a binary search to find the highest z_water that doesn't overflow.
# Calculate the volume of the newly formed reservoir.
# Do this for every local minima in the mining area, skipping minima that get
# flooded when filling other minima.


import math, pyproj, time
import numpy as np, pandas as pd, geopandas as gpd
import matplotlib.pyplot as plt

import rasterio
from rasterio.merge import merge
from rasterio.mask import mask
from scipy.spatial import qhull, ConvexHull
from scipy.ndimage.measurements import label
from shapely.geometry import Polygon, Point, mapping, box
from shapely.ops import cascaded_union
    
###############
### globals ###
###############

aster30m_dir = '/media/liam/storage/sync/gdem-tiles/'
input_fname = '../data/osm-mines/osm-aus-mines.shp'
output_shapefile = './tom-price2.shp'
output_csv = './tom-price2.csv'
minimum_volume = 0.1 # GL

# so analysis doesn't take forever, set maximum number of local minima to investigate
# (if too many, smooth elevation profile a little)
max_num_minima = 1000
geod = pyproj.Geod(ellps='WGS84')

#####################
### utility funcs ###
#####################

def latlon_scaling(lat, lon):
    _,_,m_per_lat = geod.inv(lon, lat-0.5, lon, lat+0.5) # m per degree of lat
    _,_,m_per_lon = geod.inv(lon-0.5, lat, lon+0.5, lat) # m per degree of long
    return m_per_lat, m_per_lon

def get_latlon(poly):
    bounds = poly.bounds
    lat = (bounds[1] + bounds[3])/2
    lon = (bounds[0] + bounds[2])/2
    return lat,lon

def calc_area(poly):
    # shapely is not GIS, so polygon.area calculates area on euclidean plane
    lat,lon = get_latlon(poly)
    m_per_lat, m_per_lon = latlon_scaling(lat, lon)
    return poly.area * m_per_lat * m_per_lon
    
# values: 2D numpy array
def surface_plot(values, title='', azim=0):
   # Z = np.float(values)
    Z = values
    X = np.arange(values.shape[0])
    Y = np.arange(values.shape[1])
    X,Y = np.meshgrid(X,Y,indexing='ij')
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.view_init(elev=0., azim=azim)
    ax.set_title(title)
    ax.set_xlabel('x (sec)')
    ax.set_ylabel('y (sec)')
    ax.set_zlabel('elevation (m)')
    ax.plot_surface(X,Y,Z, cmap='ocean')
    plt.show()

# each element is average of 9 neighbourhood
def smooth_elevation(AA, BB):
    AA = (
        np.roll(np.roll(AA, -1, 0), -1, 1) +
        np.roll(np.roll(AA, -1, 0),  1, 1) +
        np.roll(np.roll(AA,  1, 0), -1, 1) +
        np.roll(np.roll(AA,  1, 0),  1, 1) +
        np.roll(AA,  1, 0) +
        np.roll(AA, -1, 0) +
        np.roll(AA,  1, 1) +
        np.roll(AA, -1, 1) +
        AA
    ) / 9.0
    return AA * (1 - BB)
   
# smooth elevation profile until there are few enough local minima
# local minima must be less or equal to 8 neighbours     
def local_minima(AA, BB, MM):
    minima = np.bitwise_and(MM, (
        (AA <= np.roll(np.roll(AA, -1, 0), -1, 1)) & 
        (AA <= np.roll(np.roll(AA, -1, 0),  1, 1)) &  
        (AA <= np.roll(np.roll(AA,  1, 0), -1, 1)) &  
        (AA <= np.roll(np.roll(AA,  1, 0),  1, 1)) &
        (AA <= np.roll(AA,  1, 0)) & 
        (AA <= np.roll(AA, -1, 0)) & 
        (AA <= np.roll(AA,  1, 1)) & 
        (AA <= np.roll(AA, -1, 1))
    ))
    
    if minima.sum() > max_num_minima:
        AA = smooth_elevation(AA, BB)
        return local_minima(AA, BB, MM)
        
    return minima
    
# find aster30m tif file that covers this lat/long
def aster30m_fname(x, y):
    c1 = 'N' if y >= 0 else 'S'
    c2 = 'E' if x >= 0 else 'W'
    x = abs(math.floor(x))
    y = abs(math.ceil(y))
    return aster30m_dir + '{}{}{}{}.tif'.format(c1, y, c2, x)
    
# polygon: shapely.geometry.polygon.Polygon (mine area)
# out: numpy.ndarray of values from rasters that cover (elevation)
def load_raster_file(polygon):
    minX,minY,maxX,maxY = polygon.bounds
    minX,maxX = math.floor(minX), math.floor(maxX)
    minY,maxY = math.floor(minY), math.floor(maxY)
    fnames = set([
            aster30m_fname(minX,minY),
            aster30m_fname(minX,maxY),
            aster30m_fname(maxX,minY),
            aster30m_fname(maxX,maxY),
    ])
    srcs = [rasterio.open(fname) for fname in fnames]
    if len(srcs) == 1:
        return srcs[0]
    
    # damn we have to stitch multiple rasters together
    data,transform = merge(srcs)
    meta = srcs[0].meta.copy()
    meta.update({
            'height': data.shape[1],
            'width': data.shape[2],
            'transform': transform,
    })
    with rasterio.open('temp', 'w', **meta) as f:
        f.write(data)
    return rasterio.open('temp')
    
def load_elevation_and_mask(polygon):
    polygon_json = [mapping(polygon)] # convert to GeJSON format
    bbox_json = [mapping(box(*polygon.bounds))]
    # get bounding box as polygon, then use `mask` to crop raster to 
    # the bounding box
    # there's probably a much neater way!
    raster = load_raster_file(polygon)
    values,_ = mask(raster, polygon_json, crop=True, nodata=-1)
    area_mask = values[0,1:-1,1:-1] != -1 # True inside mining area
    values,_ = mask(raster, bbox_json, crop=True)
    values = values[0,1:-1,1:-1] # elevation for whole bounding box
    assert(values.shape == area_mask.shape)
    return values, area_mask


def calc_water_level(AA, BB, x, y):
    # fill the elevation profile (AA) with water until the local minima (x,y)
    # is connected to the boundary (BB). do a binary search between elevation
    # of minima and max elevation
    z_min, z_max = AA[x, y], np.max(AA)
    while z_max - z_min > 1:
        z_water = int((z_min+z_max)/2)
        CC,_ = label(np.bitwise_or(BB, AA <= z_water)) # CC for connected components
        
        if CC[0,0] == CC[x,y]: # overflow
            z_max = z_water
        else: # no overflow
            z_min = z_water
            
    z_water = z_min # z_water is the highest water level without overflow
    CC,_ = label(np.bitwise_or(BB, AA <= z_water)) # connected components analysis
    CC = CC == CC[x,y] # select just the water filled component
    
    # still overflowing, likely not a true minima (equal value to neighbour)
    if CC[0,0] == CC[x,y]: 
        assert(z_water == AA[x, y])
        z_water = None
        
    return CC, z_water

# this needs checking...
# a better way would be to save the transform (second return value of mask())
# or get the proper data from the raster.
def xy_to_latlon(poly, x, y):
    lat = poly.bounds[3] - x*1/3600
    lon = poly.bounds[0] + y*1/3600
    return lat,lon

# concave hull around pit points
def calc_pit_geometry(poly, depth):
    pts = np.transpose(np.array(np.where(depth>0)))
    try:
        hull = ConvexHull(pts)
    except qhull.QhullError:
        return None
    vertices = []
    for v in hull.vertices:
        vertices.append(pts[v]) 
    vertices = [xy_to_latlon(poly, x, y)[::-1] for x,y in vertices]
    return Polygon(vertices)

# make each pit point a circle, and combine into polygon
def calc_pit_geometry2(poly, depth):
    pts = np.transpose(np.array(np.where(depth>0)))
    pts = [xy_to_latlon(poly,*pt) for pt in pts]
    radius = 0.501 * 1/3600 # pixel resolution in deg
    squares = [Point(lon,lat).buffer(2*radius).envelope for lat,lon in pts]
    shape = cascaded_union(squares)
    assert(shape.type == 'Polygon') # shouldn't be mutlipolygon
    return shape

def pit_details(mine, mine_polygon, depth, z_water, volume):
    pit = mine.copy() # copy mine specific details
    pit['pit_geometry'] = calc_pit_geometry2(mine_polygon, depth)
    pit['pit_depth'] = depth.max() # m
    x,y = np.where(depth == pit['pit_depth']) # lat/lon of deepest part of pit
    x,y = x[0],y[0] # just take the first instance
    pit['pit_lat'], pit['pit_lon'] = xy_to_latlon(mine_polygon, x,y) 
    pit['pit_top'] = z_water # m of elevation
    pit['pit_bottom'] = z_water - pit['pit_depth'] # m of elevation
    pit['pit_volume'] = volume # in GL
    pit['pit_area'] = np.sum(depth > 0) * area_per_square_unit # in m2
    
    # volume as a function of water level elevation:
    for i in range(100, 0, -10):
        water_level = pit['pit_bottom'] + i/100*pit['pit_depth']
        sub_volume = np.sum(np.maximum(0, depth - (pit['pit_top']-water_level)))
        pit[str(i)+'_altitude'] = water_level
        pit[str(i)+'_volume'] = sub_volume * volume_per_cubic_unit
    
    return pit
    
############
### main ###
############
    
t0 = time.time()
shapefile = gpd.read_file(input_fname) 
pits = []

for index, row in shapefile.iterrows():

    name = str(row['name'])
    
    if 'Mount Tom Price mine' != name:
        continue
    
    poly = row.geometry
    if row.geometry == None:
        print('[*] {:4} {:40} mine area not defined'.format(index, name))
        continue
    area = calc_area(poly)
        
    mine = {} # mine specific details
    mine['mine_id'] = index # id in OSM shapefile
    mine['mine_name'] = name # name is OSM shapefile
    mine['mine_area'] = area # in m2
    
    lat,lon = get_latlon(poly)
    m_per_lat, m_per_long = latlon_scaling(lat,lon)
    elevation, area_mask = load_elevation_and_mask(poly)
    area_per_square_unit = m_per_lat/3600 * m_per_long/3600
    volume_per_cubic_unit = 1.0 * area_per_square_unit / 1e6 # GL
    #surface_plot(elevation, title=index)
    
    # AA, MM, BB, PP, CC are all 2d numpy array of the same shape...
    
    # AA is elevation profile, with a 1 pixel wide boarder of 0 elevation
    s0,s1 = elevation.shape
    AA = np.zeros((s0+2, s1+2), dtype=elevation.dtype)
    AA[1:-1,1:-1] = elevation
    
    # MM == True inside the mining area
    MM = np.zeros(AA.shape, dtype='bool')
    MM[1:-1,1:-1] = area_mask
    
    ## BB == True around the boarder (1 pixel wide)
    BB = np.ones(AA.shape, dtype='bool') # B for bounding box
    BB[1:-1, 1:-1] = False
    
    # PP == True at a local minima inside the mining area
    PP = local_minima(AA, BB, MM)

    while np.sum(PP) > 0:
        
        minima = np.where(PP)
        x, y = minima[0][0], minima[1][0] # take the first minima
        z = AA[x,y]
        
        # CC (connected component) == True inside the pit
        CC, z_water = calc_water_level(AA, BB, x, y)
        
        if z_water == None:
            PP[x,y] = False # no pit found, remove this minima
            continue
        PP = np.bitwise_and(PP, np.bitwise_not(CC)) # remove all minima in flooded area
        
        depth = CC * (z_water - AA)
        volume = np.sum(depth) * volume_per_cubic_unit
        if volume < minimum_volume:
            continue
        
        # big pit found
        #print('Minima ({},{}): from {}m to {}m. {:.2f}GL'.format(x, y, z, z_water, volume))
        #surface_plot(depth)
        pits.append(pit_details(mine, poly, depth, z_water, volume))
    

    
if len(pits) > 0:
    df = pd.DataFrame(pits)
    
    gdf = gpd.GeoDataFrame(pits, geometry='pit_geometry')
    gdf.crs = "EPSG:4326"
    gdf.to_file(output_shapefile)
    
    df = df.drop('pit_geometry', axis=1)
    df.to_csv(output_csv)
    
    print('[*] saved pits to {} and {}'.format(output_shapefile, output_csv))
    
print('[*] run time: {:.1f}s'.format(time.time() - t0))

